# Usage(linux):
    * Install python3, gstreamer with all necessary plugins via your package manager.
    * Install flask package via pip.
    * $ export FLASK_APP=main.py 
    * $ flask run --host=127.0.0.1
