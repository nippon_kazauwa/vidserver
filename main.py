from flask import Flask
from flask.globals import request
import subprocess as sp
import datetime as dt

app = Flask(__name__)

rec_holder = dict()

@app.route('/rec/start',methods=['POST'])
def rec_start():
    pid = start_rec_sh(request.get_json()['address'])
    return {'id': pid}

def start_rec_sh(link):
    process = sp.Popen(["gst-launch-1.0","-eq","rtspsrc",f"location={link}","!","queue","min-threshold-time=1000000000","!","rtph264depay","!","h264parse","!","mp4mux","!","filesink",f"location=recording_{str(dt.datetime.now().strftime('%Y%m%d-%H-%M-%S'))}.mp4","sync=false"])
    pid = process.pid

    rec_holder[pid] = link
    return pid


@app.route('/rec/stop', methods=['POST'])
def stop_rec():
    pid = request.get_json()['id']
    video_address = stop_rec_sh(pid)
    return({'video_address' : video_address})

def stop_rec_sh(pid):
    link = rec_holder[pid]
    sp.run(["/bin/kill",f"{pid}"])

    var = rec_holder[pid]
    del rec_holder[pid]
    return var
